# Sistema-almacenes-COM450

Proyecto echo en la materia com 450(calidad de software)

# Requisitos
    * Python 3.X 
    * Mysql
    * pip

# Instalacion

```
    1) pasos previos
        * pip install virtualenv
        * moverse a la carpeta donde estara el protecto
    2) crear un entorno virtual
        * python -m venv env
        * env\Scripts\activate.bat        
        ** moverse a la altura de requirements.txt **
        * pip install -r  requirements.txt
    3) Ejecutarlo
        ** moverse a la altura de manage.py **
        1) python manage.py seed # para llenar con datos(optional)
        2) python manage.py runserver
        3) digirse a http://127.0.0.1:5000/

```

